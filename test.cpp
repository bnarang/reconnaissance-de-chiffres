#include <iostream>
#include <vector>
#include "table_decision.hpp"
#include "test.hpp"

#define NB_CLASS (10)

using namespace std;

// Cette focntion
void sortSample(vector<NamedPoint> samples, vector<int>& guessedClass, table_decision& tableDecision)
{
  int side;
  vector<int> allClass (NB_CLASS, 1);


  for (int i = 0; i < samples.size(); i++)
  {
    //On prend chaque hyperplan et à chaque fois on élimine la classe à laquelle il n'appartient pas
    for (int j = 0; j < tableDecision.getSize(); j++)
    {
      side = (tableDecision.getHyperplan(j)).position(samples[i]);

      if (side==1)
        allClass[tableDecision.getClass1(j)] = 0;

      if (side==-1)
        allClass[tableDecision.getClass2(j)] = 0;
    }

    //On stocke la classe reconnue dans guessedClass
    for (int j = 0; j < allClass.size(); j++)
    {
      if (allClass[j] == 1)
      {
        //Cas d'un étiquettage multiple
        if (guessedClass[i] != -1)
        {
          guessedClass[i] = -2;
          break;
        }
        guessedClass[i] = j;
      }
    }
    allClass.clear();
    allClass.assign(NB_CLASS,1);
  }
}


Results compare_result(const vector<NamedPoint>& Samples, const vector<int>& guessedClass, int nbClass)
{
  Results res(nbClass);

  for (int i = 0; i < Samples.size(); i++)
  {
    switch(guessedClass[i])
    {
      case -2: res.incMultiLabel();break;

      case -1: res.incNoLabel(Samples[i].getEtiquette());break;

      default : res.incLabelling(Samples[i].getEtiquette(), guessedClass[i]);
    }
  }

  return res;
}

void printRes(Results res)
{
  cout << "Nombre de point avec plusieurs étiquettes :" << res.getMultiLabel() << endl;
  cout << "Nombre de point sans étiquette :";
  res.getNoLabel();
  cout << endl;


  cout << "Résultats :" << endl;
  res.getLabelling();
}