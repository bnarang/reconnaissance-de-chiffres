#include <vector> // pour utiliser les vecteurs
#include <iostream>
#include <stdlib.h> // pour utiliser exit, EXIT_FAILURE et EXIT_SUCCESS

using namespace std; // pour simplifier les écritures


/* Un produit scalaire avec une fonction*/
// Pourquoi ne pas utiliser inner_product(u, v) de la STL ? (Ben)
double dotProduct(vector<double> u, vector<double> v)
{ if (u.size() == v.size())
  { double res = 0.0;
    for (unsigned i=0; i< u.size() ; i++)
    { res += u[i]*v[i]; }
    return(res);
  }
  else
  { cerr << "wrong type of arguments in dotProduct" << endl;
    exit(EXIT_FAILURE);
  }
}


/* Calcul de la norme carrée */
double sqrNorme(vector<double> u)
{
  return(dotProduct(u,u));
}



/* Soustraction de deux vecteurs */
vector<double> operator-(vector<double> const& u, vector<double> const& v)
{  if (u.size() == v.size())
  { vector<double> res(u.size(), 0);
    for (unsigned i=0; i< u.size() ; i++)
    { res[i] = u[i] - v[i]; }
    return(res);
  }
  else
  { cerr << "wrong type of arguments in substract" << endl;
    exit(EXIT_FAILURE);
  }
}



/* Addition de deux vecteurs */
vector<double> operator+(vector<double> const& u, vector<double> const& v)
{  if (u.size() == v.size())
  { vector<double> res(u.size(), 0);
    for (unsigned i=0; i< u.size() ; i++)
    { res[i] = u[i] + v[i]; }
    return(res);
  }
  else
  { cerr << "wrong type of arguments in add" << endl;
    exit(EXIT_FAILURE);
  }
}


/* Multiplication d'un vecteur par un scalaire */
vector<double> operator*(double const& c, vector<double> const& u)
{ vector<double> res(u.size(), 0);
  for (unsigned i=0; i< u.size() ; i++)
  { res[i] = c * u[i]; }
  return(res);
}
