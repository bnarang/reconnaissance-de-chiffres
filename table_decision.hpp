#ifndef TABLE_DECISION_HPP
#define TABLE_DECISION_HPP

#include <vector> // pour utiliser les vecteurs
#include <stdlib.h> // pour utiliser exit, EXIT_FAILURE et EXIT_SUCCESS
#include "vector_fct.hpp"

using namespace std;

// Classe représentant un point avec son étiquette
class NamedPoint {
  protected:
    int etiquette;
    vector<double> point;

  public:
    NamedPoint()                           : etiquette(0), point() {}
    NamedPoint(int value, vector<double> V): etiquette(value), point(V) {}
    NamedPoint(NamedPoint const & P)       : etiquette(P.getEtiquette()), point(P.getPoint()) {}
    ~NamedPoint() {}

    vector<double> getPoint() const           { return point; }
    int  getEtiquette      () const           { return etiquette; }
    void setEtiquette      (int value)        { etiquette = value; }
    void setPoint          (vector<double> V) {
      point.clear(); // Sinon c'est pas bô
      point = V;
    }
    void clear            () { etiquette = 0; point.clear(); }
    void print            () {
      cout << getEtiquette() << " : [";
      for (int i=0; i<point.size()-1; i++) {
        cout << point[i] << ", "; }
      cout << point.back() << "]" << endl;
    }
    void copy (NamedPoint const & P) {
      point.clear();
      etiquette = P.getEtiquette();
      point = P.getPoint();
    }
};

/* Classe pour représenter un hyperplan */

class hyperplan {
  private:
    vector<double> point;    //Vecteur contenant un point de l'hyperplan
    vector<double> normal;   //Vecteur normal à l'hyperplan

  public:
    hyperplan(): point(), normal() {}
    hyperplan(vector<double> P, vector<double> N) : point(P), normal(N) {}
    ~hyperplan() {}

    vector<double> getPoint  () { return (point); }
    vector<double> getNormal () { return (normal); }

    /* Fonction indiquant où un point ce trouve par rapport à l'hyperplan
       Renvoie 1 si le point est du côté pointé par la normale
               -1 si le point est de l'autre côté
       NB : c'est peu intuitif mais ça marche à partir l'équation de l'hyperplan
    */
    int position(NamedPoint sample) {

      if (dotProduct(sample.getPoint() - point, normal) > 0)
        return 1;
      else
        return -1;
    }
};


class table_decision
{
private:
  vector<hyperplan> separateur;
  /* Un hyperplan sépare deux classes contenues dans class1 et class2 tq la normale va de class1 vers class2 */
  vector<int> class1;
  vector<int> class2;

public:
  table_decision(): class1(), class2() {}
  ~table_decision() {}

  const int getSize() {return separateur.size();}
  hyperplan getHyperplan(int nb) {return separateur[nb];}
  int getClass1(int nb) {return class1[nb];}
  int getClass2(int nb) {return class2[nb];}

  void setClass1(int nb) {class1.push_back(nb);}
  void setClass2(int nb) {class2.push_back(nb);}
  void setSeparateur(hyperplan h) {separateur.push_back(h);}

};


#endif
