#include <iostream>
#include <fstream>
#include <time.h>
#include <algorithm>
#include "ImageArray.hpp"
#include "table_decision.hpp"
#include "test.hpp"
#include "apprentissage.hpp"

#define NB_CLASS             (10)
#define NB_FILES             (10)
#define NB_OF_LINES          (32)
#define NB_OF_IMAGES_BY_LINE (20)

using namespace std;


int main(int argc, char* argv[])
{
  // Les varaibles du main
  time_t start_time = time(NULL); // On lance le chrono
  NamedPoint named_point;
  BMP image_bmp; // Va contenir le fichier bmp en cours de traitement
  ImageArray image_array; // Va contenir le caractère (64x64px) en cours

  vector<vector<NamedPoint> > appr_data(NB_CLASS);

  // On ouvre un fichier pour écrire les premiers vecteurs d'apprentissage de chaque classe :
  ofstream vector_file;
  vector_file.open("out/vectors.txt", ofstream::out | ofstream::trunc);

  // On boucle sur les fichiers appr_0.bmp ... appr_9.bmp
  cout << endl << "**********************************************" << endl
               << "* Prétraitement des fichiers d'apprentissage * (" << difftime(time(NULL), start_time) << "s)" << endl
               << "**********************************************" << endl;

  // Note : on peut aussi couper tout ça dans une fonction qu'on appelle, si on veut épurer le main.
  // Pour l'instant, je préfère qu'on voit bien ce qui se passe, ça évite de naviguer entre les fichiers
  for (int i = 0; i < NB_FILES; i ++) {
    string file_name_s    = "files/appr_" + to_string(i) + ".bmp";
    cout << "Traitement de " << file_name_s << "... (" << difftime(time(NULL), start_time) << "s)" << endl;

    // On récupère toute l'image (on cast file_name_s en const char *)
    image_bmp.ReadFromFile(file_name_s.c_str());

    // On parcours l'image, par ligne et colonnes
    for (int n = 0; n < NB_OF_LINES; n ++) {
      for (int p = 0; p < NB_OF_IMAGES_BY_LINE; p ++) {
        // On stocke le caractère
        image_array.CopyZoneFromBMP(image_bmp, 64*n, 64*(n+1), 64*p, 64*(p+1));
        // On calcul le NamedPoint correspondant
        named_point.setEtiquette(i);
        named_point.setPoint(image_array.komoriVector123());
        if (sqrNorme(named_point.getPoint())!=0)
        {
          appr_data[i].push_back(named_point);
        }else{
          break;
        }

        // On écrit dans le fichier...
        if (n == 1 and p <= 5) {
          vector_file << "\n\n###########" << named_point.getEtiquette() << "\n";
          for (int i = 0; i < named_point.getPoint().size(); i ++)
            vector_file << named_point.getPoint()[i] << "\n";
        }
      }
    }
  }

  // On ferme le fichier
  vector_file.close();


  /* En enlève les points trop éloignés du barycentre */
  // NamedPoint bar;
  // int total_size = 0;
  // vector<double> distances;
  // int indice_max;
  // int frac = 100;
  // for (int k = 0; k < appr_data.size(); k ++) {
  //   bar = barycentre(appr_data[k]);
  //   total_size = appr_data[k].size();
  //   distances.clear();
  //   for (int i = 0; i < appr_data[k].size()-1; i ++)
  //     distances.push_back(sqrNorme(bar.getPoint() - appr_data[k][i].getPoint()));
  //   for (int i = 0; i < total_size/10; i ++) {
  //     indice_max = distance(distances.begin(), max_element(distances.begin(), distances.end()));
  //     distances.erase(distances.begin() + indice_max);
  //     appr_data[k].erase(appr_data[k].begin() + indice_max);
  //   }
  // }

  /* Appel aux fonctions d'apprentissage (Sylvain)*/

  cout << endl << "**********************************" << endl
               << "* Calcul de la table de décision * (" << difftime(time(NULL), start_time) << "s)" << endl
               << "**********************************" << endl;

  table_decision tableDecision;
  create(tableDecision, appr_data);


  /* Préparation des fichiers de tests*/
  cout << endl << "************************************************" << endl
               << "* Prétraitement des fichiers de reconnaissance * (" << difftime(time(NULL), start_time) << "s)" << endl
               << "************************************************" << endl;
  vector<NamedPoint> samples;

  for (int i = 0; i < NB_FILES; i ++) {
    string file_name_s    = "files/rec_" + to_string(i) + ".bmp";
    cout << "Traitement de " << file_name_s << "... (" << difftime(time(NULL), start_time) << "s)" << endl;

    // On récupère toute l'image (on cast file_name_s en const char *)
    image_bmp.ReadFromFile(file_name_s.c_str());

    // On parcours l'image, par ligne et colonnes
    for (int n = 0; n < NB_OF_LINES; n ++) {
      for (int p = 0; p < NB_OF_IMAGES_BY_LINE; p ++) {
        // On stocke le caractère
        image_array.CopyZoneFromBMP(image_bmp, 64*n, 64*(n+1), 64*p, 64*(p+1));
        // On calcul le NamedPoint correspondant
        named_point.setEtiquette(i);
        named_point.setPoint(image_array.komoriVector123());
        // On peut l'utiliser ...
        if (sqrNorme(named_point.getPoint())!=0)
        {
          samples.push_back(named_point);
        }else{
          break;
        }
      }
    }
  }


  /* Appel aux fonctions de tests (Adrien)*/
  cout << endl << "*******************************************" << endl
               << "* Utilisation de la table d'apprentissage * (" << difftime(time(NULL), start_time) << "s)" << endl
               << "*******************************************" << endl;

  // Création du vecteur qui contiendra les classes reconnues
  vector<int> guessedClass (samples.size(), -1);

  // "Remplissage" de guessedClass
  sortSample(samples, guessedClass, tableDecision);  //samples est du type vector<namedPoint> à mofifier suivant ce que Benjamin nous fait

  cout << endl << "*************************" << endl
               << "* Analyse des résultats * (" << difftime(time(NULL), start_time) << "s)" << endl
               << "*************************" << endl;
  // Comparaison des classes reconnues avec les classes réelles
  Results res;
  res = compare_result(samples, guessedClass, NB_CLASS);

  printRes(res);


  /* Test d'implémentation de la méthode des PPV */
  // PPVMethod(samples, guessedClass, appr_data);
  // cout << endl << "*************************" << endl
  //              << "* Analyse des résultats via PPV * (" << difftime(time(NULL), start_time) << "s)" << endl
  //              << "*************************" << endl;
  // Comparaison des classes reconnues avec les classes réelles


  // res = compare_result(samples, guessedClass, NB_CLASS);

  // printRes(res);

  return 0;
}
