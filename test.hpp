#ifndef TEST_HPP
#define TEST_HPP

#include <iomanip>
#include "vector_fct.hpp"

void sortSample(vector<NamedPoint> Samples, vector<int>& guessedClass, table_decision& tableDecision);

/* Classe permettant de stocker les résultats*/
class Results
{
private:
  vector<int> noLabel;
  int multiLabel;
  vector<vector<int>> labelling;

public:
  Results (): noLabel(), multiLabel(0), labelling() {}
  Results (int nbClass): noLabel(nbClass, 0), multiLabel(0) {
    vector<int> line(nbClass, 0);
    labelling.assign(nbClass, line);
  }

  ~Results () {}

  int getSize() {return labelling.size();}

  void incNoLabel(int trueClass) {noLabel[trueClass]++;}
  void incMultiLabel() {multiLabel++;}
  void incLabelling(int trueClass, int guessedClass) {labelling[trueClass][guessedClass]++;}

  void getNoLabel() {
    int sum = 0;

    for (int i=0; i<noLabel.size(); i++)
      sum += noLabel[i];

    cout << sum << " (";
    for (int i=0; i<noLabel.size(); i++)
      cout << noLabel[i] << ", ";

    cout << ")" << endl;
  }
  int getMultiLabel() {return multiLabel;}
  void getLabelling() {

    for (int i= 0; i<labelling.size(); i++)
    {
      int nbSamples=noLabel[i];

      cout << "Classe " << i << " :" << setw(4);
      for (int j = 0; j<labelling.size(); j++)
      {
        cout << labelling[i][j] << " |" << setw(4);
        nbSamples += labelling[i][j];
      }


      cout << fixed;
      cout << setprecision(1);
      cout << "( Bon : " << (double)labelling[i][i]/(double)nbSamples * 100 << "% )" << endl;
    }
  }

};


Results compare_result(const vector<NamedPoint>& Samples, const vector<int>& guessedClass, int nbClass);

void printRes(Results res);

#endif