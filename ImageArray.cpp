#include <iostream>
#include <vector>
#include <array>
#include <list>
#include <algorithm>
#include "ImageArray.hpp"

using namespace std;

// Cette fonction génère les 41 caractères de "l'alphabet" des labels possibles pour un pixel.
// On comment par le 8dirs avec des noirs tout autour,
// ensuite on a les 8 labels avec un trou, d'abord en 8, puis en 1, etc.
// puis les 8 labels avec deux trous en 0 et 1, etc.
// Cela fait 41 labels
array<Label, 41> create_alphabet() {
  array<Label, 41> alphabet;
  Label config;
  config.fill(true);
  alphabet[0] = config;

  for (int i = 0; i < 5; i ++) {
    for (int j = 0; j < 8; j ++) {
      config.fill(true);
      for (int k = j; k < j+i+1; k ++)
        config[k % 8] = false;
      alphabet[8*i + j+1] = config;
    }
  }

  return alphabet;
}


/* *****************************************
   Les fonctions pour la class ImageArray
***************************************** */

// Définir à partir d'une class BMP sur une zone (i=position verticale, j=position horizontale)
void ImageArray::CopyZoneFromBMP(const BMP& aBMP, int i_start, int i_end, int j_start, int j_end) {
  // Vérification de la zone qu'on veut copier
  if (i_start < 0) {
    i_start = 0;
    cout << "Attention : départ de la zone négatif." << endl;
  }
  if (j_start < 0) {
    j_start = 0;
    cout << "Attention : départ de la zone négatif." << endl;
  }
  if (i_end > aBMP.TellHeight()) {
    i_end = aBMP.TellHeight();
    cout << "Attention : tentative de copie d'une zone plus haute que l'image." << endl;
  }
  if (j_end > aBMP.TellWidth()) {
    j_end = aBMP.TellWidth();
    cout << "Attention : tentative de copie d'une zone plus large que l'image." << endl;
  }

  // On enlève la première ligne et la première colonne de blancs
  i_start ++;
  j_start ++;

  // La copie
  vector<vector<Pixel>> vect_image;
  vector<Pixel> line;
  // On parcourt tous les pixels, en regardant la couleur
  // Normalement, dans notre cas, on est soit (255, 255, 255)=blanc soit (0, 0, 0)=noir
  for (int i = i_start; i < i_end; i ++) {
    line.clear();
    for (int j = j_start; j < j_end; j ++) {
      if ((int)aBMP.GetPixel(j,i).Green == 255)
        line.push_back(White);
      else if ((int)aBMP.GetPixel(j,i).Green == 0)
        line.push_back(Black);
      else
        cout << "Erreur ! Ce pixel n'est ni noir ni blanc (" << i << ", " << j << ")." << endl;
    }
    vect_image.push_back(line);
  }

  // On supprime la a dernière ligne et la dernière colonne, tant qu'on ne rencontre pas de noirs
  // On rogne le haut et le base
  // while (vect_image[0] == empty_line)
  //   vect_image.erase(vect_image.begin());
  // while (image[vect_image.size()-1] == empty_line)
  // vect_image.pop_back();
  for (int i = 0; i < vect_image.size(); i ++) {
    for (int j = 0; j < vect_image[i].size(); j ++)
      cout << vect_image[i][j] << " ";
    cout << endl;
  }
  // vect_image[0].pop_back();
  vect_image.pop_back();

  image.clear();
  image = vect_image;
}

int ImageArray::getHeight() {
  if (not image.empty())
    return image.size();
  else
    return 0;
}

int ImageArray::getWidth() {
  if (not image.empty())
    return image[0].size();
  else
    return 0;
}

// Accéder à la couleur d'un pixel
Pixel ImageArray::getPixel(int i, int j) {
  if (0 <= i < getHeight() and 0 <= j < getWidth())
    return image[i][j];
  else {
    cout << "Erreur, tentative de lecture d'un élément hors du tableau." << endl;
    return White;
  }
}

// Définir la couleur d'un pixel
void ImageArray::setPixel(int i, int j, Pixel color) {
  if (0 <= i < getHeight() and 0 <= j < getWidth())
    image[i][j] = color;
  else
    cout << "Erreur, tentative d'écriture d'un élément hors du tableau." << endl;
}


/* ********************************************
   Les fonctions pour l'algorithme de Komori
******************************************** */

// Indique si le pixel (i,j) a un pixel noir dans la direction indiquée
bool ImageArray::hasBlackOn(int i, int j, int direction) {
  int height = getHeight();
  int width  = getWidth();
  int k      = i;
  int l      = j;
  int k_move = 0;
  int l_move = 0;

  // On définit dans quel sens on se déplace,
  // en fonction de la direction indiquée
  switch (direction) {
    case 0:
      k_move = -1;
      break;
    case 1:
      k_move = -1;
      l_move = 1;
      break;
    case 2:
      l_move = 1;
      break;
    case 3:
      k_move = 1;
      l_move = 1;
      break;
    case 4:
      k_move = 1;
      break;
    case 5:
      k_move = 1;
      l_move = -1;
      break;
    case 6:
      l_move = -1;
      break;
    case 7:
      k_move = -1;
      l_move = -1;
      break;
    default:
      cout << "Erreur : ceci n'est pas une direction." << endl;
  }

  // On se déplace dans la direction, et on regarde s'il y a un noir...
  while (0 <= k and k < height and 0 <= l and l < width) {
    if (getPixel(k,l) == Black)
      return true;
    k += k_move;
    l += l_move;
  }

  return false;
}

// komoriLabel = return la liste des directions où il y a un noir
Label ImageArray::komoriLabel(int i, int j) {
  Label komori_label;
  komori_label.fill(false);

  // On regarde s'il y a un noir dans chaque direction,
  // sauf si le pixel est noir (dans ce cas, tout est false)
  if (getPixel(i, j) == White) {
    for (int k = 0; k < 8; k ++)
      komori_label[k] = hasBlackOn(i, j, k);
  }

  return komori_label;
}

// Permet de 'relaxer' le résultat : on modifie de proche en proche les label,
// lorsque certains labels se trouvent à côté d'autres.
// Les points ayant pour lesquels la list komoriLabel est 8xfalse sont à ignorer :
// Se sont soit des points du bord, soit des points noirs.
vector<vector<Label>> ImageArray::komoriRelaxedArray() {
  int height = getHeight();
  int width  = getWidth();

  array<Label, 41> alphabet(create_alphabet());
  vector<Label> line(width, alphabet[0]);
  vector<vector<Label>> komori_array(height, line);

  for (int i=0; i<height; i++) {
    for (int j=0; j<width; j++)
      komori_array[i][j] = komoriLabel(i, j);
  }

  // Relaxation

  // Les variables
  array<Label, 4> neighbours;
  vector<vector<Label>> komori_array_relax(komori_array);
  Label true_array;
  Label false_array;
  true_array.fill(true);
  false_array.fill(false);
  int nb_iter(0);

  // L'algorithme : on applique les modifications tant que le tableau change (pas stabilisé)
  do {
    nb_iter ++;
    // cout << "  " << nb_iter << endl;
    komori_array = komori_array_relax;

    for (int i = 1; i < height-1; i ++) {
      for (int j = 1; j < width-1; j ++) {
        // Les règles de relaxation : si on a des noirs dans toutes les directions
        if (komori_array[i][j] == true_array) {
          neighbours[0] = komori_array[i-1][j];
          neighbours[1] = komori_array[i+1][j];
          neighbours[2] = komori_array[i][j-1];
          neighbours[3] = komori_array[i][j+1];
          // si l'un des neighbours n'est pas entouré de noir, son label se "transmet" au point
          // for (int k = 0; k < 4; k ++) {
          //   if (neighbours[k] != true_array and neighbours[k] != false_array)
          //     komori_array_relax[i][j] = neighbours[k];
          // }
          // on regroupe certaines classes
          for (int k = 0; k < 4; k ++) {
            if (neighbours[k] == alphabet[1] or neighbours[k] == alphabet[2])
              komori_array_relax[i][j] = alphabet[1];
            else if (neighbours[k] == alphabet[3] or neighbours[k] == alphabet[4])
              komori_array_relax[i][j] = alphabet[3];
            else if (neighbours[k] == alphabet[5] or neighbours[k] == alphabet[6])
              komori_array_relax[i][j] = alphabet[5];
            else if (neighbours[k] == alphabet[7] or neighbours[k] == alphabet[7])
              komori_array_relax[i][j] = alphabet[7];
            else if (neighbours[k] == alphabet[18] or neighbours[k] == alphabet[17] or
                     neighbours[k] == alphabet[10] or neighbours[k] == alphabet[11])
              komori_array_relax[i][j] = alphabet[18];
            else if (neighbours[k] == alphabet[20] or neighbours[k] == alphabet[19] or
                     neighbours[k] == alphabet[12] or neighbours[k] == alphabet[13])
              komori_array_relax[i][j] = alphabet[20];
            else if (neighbours[k] == alphabet[22] or neighbours[k] == alphabet[21] or
                     neighbours[k] == alphabet[14] or neighbours[k] == alphabet[15])
              komori_array_relax[i][j] = alphabet[22];
            else if (neighbours[k] == alphabet[24] or neighbours[k] == alphabet[23] or
                     neighbours[k] == alphabet[9] or neighbours[k] == alphabet[16])
              komori_array_relax[i][j] = alphabet[24];
            else if (neighbours[k] == alphabet[33] or neighbours[k] == alphabet[34] or
                     neighbours[k] == alphabet[25] or neighbours[k] == alphabet[26])
              komori_array_relax[i][j] = alphabet[33];
            else if (neighbours[k] == alphabet[35] or neighbours[k] == alphabet[36] or
                     neighbours[k] == alphabet[27] or neighbours[k] == alphabet[28])
              komori_array_relax[i][j] = alphabet[35];
            else if (neighbours[k] == alphabet[37] or neighbours[k] == alphabet[38] or
                     neighbours[k] == alphabet[29] or neighbours[k] == alphabet[30])
              komori_array_relax[i][j] = alphabet[37];
            else if (neighbours[k] == alphabet[39] or neighbours[k] == alphabet[40] or
                     neighbours[k] == alphabet[31] or neighbours[k] == alphabet[32])
              komori_array_relax[i][j] = alphabet[39];
          }
        }
      }
    }
  } while (komori_array != komori_array_relax);


  return komori_array;
}

// Komori pour les 8 directions :
// retourne un vecteur dans R^123
vector<double> ImageArray::komoriVector123() {
  vector<vector<Label>> komori_array_relax(komoriRelaxedArray());

  vector<double> komori_vector;
  array<Label, 41> alphabet(create_alphabet());

  int    height = getWidth();
  int    width  = getHeight();
  double height_factor = 64.0 / ((double)height);
  double width_factor  = 64.0 / ((double)width);

  vector<vector<Label>> zone;
  for (int k = 0; k < 8; k ++) {
    for (int i = k * height / 8; i < (k + 1) * height / 8; i ++)
      zone.push_back(komori_array_relax[i]);

    // On calcul la partie du vecteur correspondant à la zone
    for (int i = 0; i < 41; i ++)
      komori_vector.push_back(numberOfLabelInZone(alphabet[i], zone) * height_factor * width_factor);

    // On reset le vecteur zone
    zone.clear();
  }

  return komori_vector;
}

// Calcul du nombre de A, ou B, ... dans une des trois zones de l'image
int ImageArray::numberOfLabelInZone(Label label, const vector<vector<Label>>& zone) {
  int nb = 0;

  for (int i = 0; i < zone.size(); i ++) {
    for (int j = 0; j < zone[0].size(); j ++)
      if (zone[i][j] == label)
        nb ++;
  }

  return nb;
}
