#ifndef IMAGEARRAY_HPP
#define IMAGEARRAY_HPP

#include "EasyBMP.hpp"
#include <vector>
#include <list>
#include <array>

using namespace std;

enum Pixel {White, Black};
typedef array<bool, 8> Label; // Ex pour une concavité : [true, true, true, true, true, true, true, true]


class ImageArray {
  private:
    // L'image
    vector<vector<Pixel>> image;

    // Komori
    bool hasBlackOn(int i, int j, int direction);
    Label komoriLabel(int i, int j);
    vector<vector<Label>> komoriRelaxedArray();
    int numberOfLabelInZone(Label label, const vector<vector<Label>>& zone);

  public:
    // Constructeurs
    ImageArray() {};

    // Accès et écriture des éléments
    int   getWidth();
    int   getHeight();
    Pixel getPixel(int i, int j);
    void  setPixel(int i, int j, Pixel color);
    void  CopyZoneFromBMP(const BMP& aBMP, int i_start, int i_end, int j_start, int j_end);

    // Komori
    vector<double> komoriVector123();
};


#endif
