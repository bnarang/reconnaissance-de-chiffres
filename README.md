# Reconnaissance de chiffres

##Benjamin N. - Sylvain F. - Adrien L.

2013

Le but de ce projet est de reconnaître des chiffres manuscrits. Une base d'images est utilisée pour l'apprentissage, l'autre pour la reconnaissance.
Un algorithme de séparation linéaire, appliquée à la méthode de Komori (à 4 et 8 directions) a été utilisé.

Une bibliothèque externe a été utilisée pour la manipulation des images (fournies en BITMAP).