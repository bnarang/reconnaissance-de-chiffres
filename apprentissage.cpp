#include <iostream>
#include <vector>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <array>

#include "table_decision.hpp"
#include "vector_fct.hpp"

#define NB_K 150     // nombre de points sur lesquels moyenner
#define NB_CLASSE 10 // nombre de classes, a priori 10..
#define NB_VOIS 10 // nombre de plus proches voisins
using namespace std;


/*------------------- Deuxième méthode moins sensible au bruit --------------*/

/* Fonction de détermination de la "zone de séparation"
 * Arguments :
 * -- classe est la classe étudiée
 * -- A est le point précédent de l'algorithme
 * -- L la liste des points de la "zone de séparation"
 * -- B est le point depuis lequel on détermine la "zone de séparation"
 * renvoie true s'il y a au moins k éléments dans cette zone
 */

bool sepZone(vector<NamedPoint> classe, vector<NamedPoint> & L, NamedPoint A, NamedPoint B, vector<double> rightSens)
{ vector<double> ab = B.getPoint() - A.getPoint();

  if (dotProduct(rightSens, ab) < 0)
  {
   return false;
  }

  for (unsigned i=0; i<classe.size() ; i++)
  { if(dotProduct(ab, classe[i].getPoint() - A.getPoint()) > 0)
    { L.push_back(classe[i]);
    }
  }

  if (L.size() > NB_K)
    return(true);
  else
    return(false);
}


/* Fonction de calcul de l'iso barycentre de la "zone de séparation" */
NamedPoint barycentre(vector<NamedPoint> L)
{ /* Initialisation avec L[0] */
  NamedPoint res(L[0].getEtiquette(), L[0].getPoint());

  vector<double> tmp(L[0].getPoint().size());
  for (unsigned i=1; i<L.size(); i++)
  { tmp = res.getPoint() + L[i].getPoint();
    res.setPoint(tmp);
  }

  tmp = (1.0/L.size()) * res.getPoint();
  res.setPoint(tmp);

  return(res);
}


/* Fonction de séparation de deux classes via calcul du barycentre */
hyperplan separation(vector<NamedPoint> A, vector<NamedPoint> B)
{ NamedPoint a = barycentre(A);
  NamedPoint b = barycentre(B);

  vector<NamedPoint> L,M;

  /* Définition du sens "naturel" de la séparation */
  vector<double> rightSens = b.getPoint() - a.getPoint();

  // /* Boucle tant que les barycentres ne sont pas déterminés */
  while (sepZone(A, L, a, b, rightSens) && sepZone(B, M, b, a,rightSens))
  {
    rightSens = b.getPoint() - a.getPoint();

    a.setPoint((barycentre(L)).getPoint());
    b.setPoint((barycentre(M)).getPoint());

    L.clear();
    M.clear();
  }

  vector<double> ab = rightSens;

  /* Avertissement en cas de vecteur normal nul */
  if (sqrNorme(ab) == 0)
  { cerr << "Warning: classes must be distinct!" <<endl;
    exit(EXIT_FAILURE);
  }

  vector<double> P = a.getPoint() + (0.5 * ab);
  vector<double> N = 1.0/sqrt(sqrNorme(ab)) * ab;
  hyperplan H(P, N);
  return(H);
}


/*--------------------- Création de la table de décision ---------------------*/

/* Cette fonction prend en argument un vecteur qui contient l'ensemble des
 * données extraites des fichiers.
 */

void create(table_decision& tableDecision, vector<vector<NamedPoint> >& data)
{ /* Parcours de l'ensemble des namedpoint issu du traitement des fichiers */

  for (int i=0; i<NB_CLASSE; i++)
  {
    for (int j=i+1; j<NB_CLASSE; j++)
    {
      tableDecision.setClass1(i);
      tableDecision.setClass2(j);
      tableDecision.setSeparateur(separation(data[i],data[j]));
    }
  }
}

/*---------------------------------- PPVMethod ------------------------------*/

void PPVMethod(vector<NamedPoint>& samples, vector<int>& guessedClass, vector<vector<NamedPoint>> & data)
{ array<double,2> tmp;
  array<double,2> ini;
  ini.fill(-1);
  vector<array<double,2>> d(NB_VOIS, ini);
  array<int,NB_CLASSE> PPV;
  int tmp_max;

  for (int k=0; k<samples.size(); k++)
  {
    PPV.fill(0);
    d.assign(NB_VOIS,ini);
    for (int i=0; i<NB_CLASSE; i++)
    {
      for (int j=0; j< data[i].size(); j++)
      {
        tmp[0] = sqrNorme(samples[k].getPoint()-data[i][j].getPoint());

        for (int l = 0; l < d.size(); l++)
        {
          if (tmp[0] < d[NB_VOIS-1][0] || d[NB_VOIS-1][0]==-1)
          {
            tmp[1]=i;
            d.insert(d.begin()+l,tmp);
            d.pop_back();
            break;
          }
        }
      }
    }

    for (int i=0; i<d.size(); i++)
    {
      PPV[d[i][1]]+=1;
    }



    tmp_max = 0;
    for (int i = NB_CLASSE-1; i >= 0; i--)
    {
      if (tmp_max<=PPV[i])
      {
        tmp_max=PPV[i];
        guessedClass[k] = i;
      }
    }

  }

}