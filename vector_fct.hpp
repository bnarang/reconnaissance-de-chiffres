#ifndef VECTOR_FCT_HPP
#define VECTOR_FCT_HPP

using namespace std;

/* Norme et produit scalaire */
double dotProduct(vector<double> u, vector<double> v);
double sqrNorme(vector<double> u);

/* Addition, soustraction et multiplication par un scalaire */
vector<double> operator-(vector<double> const& u, vector<double> const& v);
vector<double> operator+(vector<double> const& u, vector<double> const& v);
vector<double> operator*(double const& c, vector<double> const& u);

#endif

