#ifndef APPRENTISSAGE_HPP
#define APPRENTISSAGE_HPP

#include <vector>
#include <list>
#include "EasyBMP.hpp"
#include "table_decision.hpp"

using namespace std;


void create(table_decision& tableDecision, vector<vector<NamedPoint> >& data);
void PPVMethod(vector<NamedPoint>& samples, vector<int>& guessedClass, vector<vector<NamedPoint>> & data);

NamedPoint barycentre(vector<NamedPoint> L);

#endif
