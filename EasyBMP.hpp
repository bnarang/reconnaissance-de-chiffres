#ifndef EasyBMP
#define EasyBMP

#include <iostream>
#include <cmath>
#include <cctype>
#include <cstring>
#include <cstdio>

#define DefaultXPelsPerMeter 3780
#define DefaultYPelsPerMeter 3780

typedef unsigned char  ebmpBYTE;
typedef unsigned short ebmpWORD;
typedef unsigned int   ebmpDWORD;
typedef struct RGBApixel {
  ebmpBYTE Blue;
  ebmpBYTE Green;
  ebmpBYTE Red;
  ebmpBYTE Alpha;
} RGBApixel;

class BMFH {
  public:
    ebmpWORD  bfType;
    ebmpDWORD bfSize;
    ebmpWORD  bfReserved1;
    ebmpWORD  bfReserved2;
    ebmpDWORD bfOffBits;
    BMFH();
    void display( void );
    void SwitchEndianess( void );
};

class BMIH{
  public:
    ebmpDWORD biSize;
    ebmpDWORD biWidth;
    ebmpDWORD biHeight;
    ebmpWORD  biPlanes;
    ebmpWORD  biBitCount;
    ebmpDWORD biCompression;
    ebmpDWORD biSizeImage;
    ebmpDWORD biXPelsPerMeter;
    ebmpDWORD biYPelsPerMeter;
    ebmpDWORD biClrUsed;
    ebmpDWORD biClrImportant;
    BMIH();
    void display( void );
    void SwitchEndianess( void );
};

BMFH GetBMFH(const char* szFileNameIn);
BMIH GetBMIH(const char* szFileNameIn);
void DisplayBitmapInfo (const char* szFileNameIn);
int GetBitmapColorDepth(const char* szFileNameIn);

class BMP {
  private:
    int BitDepth;
    int Width;
    int Height;
    RGBApixel** Pixels;
    RGBApixel* Colors;
    int XPelsPerMeter;
    int YPelsPerMeter;
    ebmpBYTE* MetaData1;
    int SizeOfMetaData1;
    ebmpBYTE* MetaData2;
    int SizeOfMetaData2;
    bool Read32bitRow(ebmpBYTE* Buffer, int BufferSize, int Row);
    bool Read24bitRow(ebmpBYTE* Buffer, int BufferSize, int Row);
    bool Read8bitRow (ebmpBYTE* Buffer, int BufferSize, int Row);
    bool Read4bitRow (ebmpBYTE* Buffer, int BufferSize, int Row);
    bool Read1bitRow (ebmpBYTE* Buffer, int BufferSize, int Row);
    bool Write32bitRow(ebmpBYTE* Buffer, int BufferSize, int Row);
    bool Write24bitRow(ebmpBYTE* Buffer, int BufferSize, int Row);
    bool Write8bitRow (ebmpBYTE* Buffer, int BufferSize, int Row);
    bool Write4bitRow (ebmpBYTE* Buffer, int BufferSize, int Row);
    bool Write1bitRow (ebmpBYTE* Buffer, int BufferSize, int Row);
    ebmpBYTE FindClosestColor(RGBApixel& input);

  public:
    int TellBitDepth      (void);
    int TellBitDepth      (void) const;
    int TellWidth         (void);
    int TellWidth         (void) const;
    int TellHeight        (void);
    int TellHeight        (void) const;
    int TellNumberOfColors(void);
    void SetDPI           (int HorizontalDPI, int VerticalDPI);
    int TellVerticalDPI   (void);
    int TellHorizontalDPI (void);
    BMP();
    BMP( BMP& Input );
    ~BMP();
    RGBApixel* operator() (int i,int j);
    RGBApixel GetPixel(int i, int j) const;
    bool SetPixel     (int i, int j, RGBApixel NewPixel);
    bool CreateStandardColorTable(void);
    bool SetSize     (int NewWidth, int NewHeight);
    bool SetBitDepth (int NewDepth);
    bool WriteToFile (const char* FileName);
    bool ReadFromFile(const char* FileName);
    RGBApixel GetColor(int ColorNumber);
    bool SetColor     (int ColorNumber, RGBApixel NewColor);
};

void PixelToPixelCopy(BMP& From, int FromX, int FromY,
                      BMP& To, int ToX, int ToY);

void PixelToPixelCopyTransparent(BMP& From, int FromX, int FromY,
                                 BMP& To, int ToX, int ToY,
                                 RGBApixel& Transparent);

void RangedPixelToPixelCopy(BMP& From, int FromL , int FromR, int FromB, int FromT,
                            BMP& To, int ToX, int ToY);

void RangedPixelToPixelCopyTransparent(BMP& From, int FromL , int FromR, int FromB, int FromT,
                                       BMP& To, int ToX, int ToY ,
                                       RGBApixel& Transparent);

bool CreateGrayscaleColorTable(BMP& InputImage);

bool Rescale(BMP& InputImage , char mode, int NewDimension);

inline double Square( double number )
{ return number*number; }

inline int IntSquare( int number )
{ return number*number; }

int IntPow( int base, int exponent );

inline bool IsBigEndian()
{
 short word = 0x0001;
 if((*(char *)& word) != 0x01 )
 { return true; }
 return false;
}

inline ebmpWORD FlipWORD( ebmpWORD in )
{ return ( (in >> 8) | (in << 8) ); }

inline ebmpDWORD FlipDWORD( ebmpDWORD in )
{
 return ( ((in&0xFF000000)>>24) | ((in&0x000000FF)<<24) |
          ((in&0x00FF0000)>>8 ) | ((in&0x0000FF00)<<8 )   );
}

bool SafeFread( char* buffer, int size, int number, FILE* fp );
bool EasyBMPcheckDataSize( void );

void SetEasyBMPwarningsOff (void);
void SetEasyBMPwarningsOn  (void);
bool GetEasyBMPwarningState(void);

#endif
